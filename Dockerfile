FROM ddosguard/php72
ENV LC_ALL=C.UTF-8 TZ=Europe/Moscow DEBIAN_FRONTEND=noninteractive

ADD file/30-xhprof.ini /etc/php/7.2/cli/conf.d/30-xhprof.ini
ADD file/30-xhprof.ini /etc/php/7.2/fpm/conf.d/30-xhprof.ini
#ADD file/php-xhprof-extension /tmp/php-xhprof-extension
ADD file/xhprof /tmp/xhprof

RUN set -x\
  && apt update -qq\
  && apt install -y --auto-remove\
      php7.2-xdebug\
      php7.2-dev\
      graphviz\
      mc\
  && mkdir /var/tmp/xhprof\
  && echo 'xdebug.remote_enable=on'>> /etc/php/7.2/fpm/php.ini\
  && cd /tmp/xhprof/extension\
  && phpize\
  && ./configure\
  && make\
  && make install\
  && apt-get autoremove -y \
  && rm -fr /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt/archives/*
